package ru.pisarev.tm.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * &lt;p&gt;Java class for addTaskAll complex type.
 * <p>
 * &lt;p&gt;The following schema fragment specifies the expected content contained within this class.
 * <p>
 * &lt;pre&gt;
 * &amp;lt;complexType name="addTaskAll"&amp;gt;
 * &amp;lt;complexContent&amp;gt;
 * &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 * &amp;lt;sequence&amp;gt;
 * &amp;lt;element name="session" type="{http://endpoint.tm.pisarev.ru/}sessionDto" minOccurs="0"/&amp;gt;
 * &amp;lt;element name="collection" type="{http://endpoint.tm.pisarev.ru/}taskDto" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 * &amp;lt;/sequence&amp;gt;
 * &amp;lt;/restriction&amp;gt;
 * &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addTaskAll", propOrder = {
        "session",
        "collection"
})
public class AddTaskAll {

    protected SessionDto session;
    protected List<TaskDto> collection;

    /**
     * Gets the value of the session property.
     *
     * @return possible object is
     * {@link SessionDto }
     */
    public SessionDto getSession() {
        return session;
    }

    /**
     * Sets the value of the session property.
     *
     * @param value allowed object is
     *              {@link SessionDto }
     */
    public void setSession(SessionDto value) {
        this.session = value;
    }

    /**
     * Gets the value of the collection property.
     * <p>
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the collection property.
     * <p>
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     * getCollection().add(newItem);
     * &lt;/pre&gt;
     * <p>
     * <p>
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link TaskDto }
     */
    public List<TaskDto> getCollection() {
        if (collection == null) {
            collection = new ArrayList<TaskDto>();
        }
        return this.collection;
    }

}
