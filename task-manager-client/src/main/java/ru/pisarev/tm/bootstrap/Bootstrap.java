package ru.pisarev.tm.bootstrap;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.pisarev.tm.api.IPropertyService;
import ru.pisarev.tm.api.repository.ICommandRepository;
import ru.pisarev.tm.api.service.ICommandService;
import ru.pisarev.tm.api.service.ILogService;
import ru.pisarev.tm.api.service.ServiceLocator;
import ru.pisarev.tm.command.AbstractCommand;
import ru.pisarev.tm.component.FileScanner;
import ru.pisarev.tm.constant.TerminalConst;
import ru.pisarev.tm.endpoint.*;
import ru.pisarev.tm.exception.system.UnknownCommandException;
import ru.pisarev.tm.repository.CommandRepository;
import ru.pisarev.tm.service.CommandService;
import ru.pisarev.tm.service.LogService;
import ru.pisarev.tm.service.PropertyService;
import ru.pisarev.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

import static ru.pisarev.tm.util.SystemUtil.getPID;
import static ru.pisarev.tm.util.TerminalUtil.displayWait;
import static ru.pisarev.tm.util.TerminalUtil.displayWelcome;

@Getter
public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final AdminEndpointService adminEndpointService = new AdminEndpointService();

    @NotNull
    private final AdminEndpoint adminEndpoint = adminEndpointService.getAdminEndpointPort();

    @NotNull
    private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();

    @NotNull
    private final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    private final TaskEndpointService taskEndpointService = new TaskEndpointService();

    @NotNull
    private final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();

    @NotNull
    private final DataEndpointService dataEndpointService = new DataEndpointService();

    @NotNull
    private final DataEndpoint dataEndpoint = dataEndpointService.getDataEndpointPort();

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    @Nullable
    private static SessionDto session;

    @Nullable
    public SessionDto getSession() {
        return session;
    }

    @Override
    public void setSession(SessionDto session) {
        Bootstrap.session = session;
    }

    public void start(String... args) {
        displayWelcome();
        initCommands();
        if (runArgs(args)) System.exit(0);
        process();
    }

    public void initApplication() {
        initPID();
    }


    @SneakyThrows
    private void initCommands() {
        @NotNull final Reflections reflections = new Reflections("ru.pisarev.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(ru.pisarev.tm.command.AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            final boolean isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            registry(clazz.newInstance());
        }
    }


    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();

    }

    private boolean runArgs(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final AbstractCommand command = commandService.getCommandByArg(args[0]);
        if (command == null) throw new UnknownCommandException(args[0]);
        command.execute();
        return true;
    }

    public void runCommand(@Nullable final String command) {
        if (command == null || command.isEmpty()) return;
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new UnknownCommandException(command);
        abstractCommand.execute();
    }

    private void registry(@Nullable AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void process() {
        logService.debug("Test environment.");
        @Nullable String command = "";
        fileScanner.init();
        while (!TerminalConst.CMD_EXIT.equals(command)) {
            try {
                displayWait();
                command = TerminalUtil.nextLine();
                logService.command(command);
                runCommand(command);
                logService.info("Completed");
            } catch (Exception e) {
                logService.error(e);
            }
        }
    }

}
