package ru.pisarev.tm.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.exception.AbstractException;

public class EmptyNameException extends AbstractException {

    @NotNull
    public EmptyNameException() {
        super("Error. Name is empty");
    }

}
