package ru.pisarev.tm.command.data;

import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.command.AuthAbstractCommand;

public class DataBase64LoadCommand extends AuthAbstractCommand {

    @Nullable
    public String name() {
        return "data-load-base64";
    }

    @Nullable
    public String arg() {
        return null;
    }

    @Nullable
    public String description() {
        return "Load base64 data";
    }

    public void execute() {
        serviceLocator.getDataEndpoint().loadDataBase64(getSession());
    }

}