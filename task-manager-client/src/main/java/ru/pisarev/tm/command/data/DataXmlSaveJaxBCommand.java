package ru.pisarev.tm.command.data;

import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.command.AuthAbstractCommand;

public class DataXmlSaveJaxBCommand extends AuthAbstractCommand {

    @Nullable

    public String name() {
        return "data-save-xml-j";
    }

    @Nullable

    public String arg() {
        return null;
    }

    @Nullable

    public String description() {
        return "Save data to XML by JaxB.";
    }

    public void execute() {
        serviceLocator.getDataEndpoint().saveDataXmlJaxB(getSession());
    }

}