package ru.pisarev.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.endpoint.ProjectDto;
import ru.pisarev.tm.enumerated.Status;
import ru.pisarev.tm.exception.empty.EmptyNameException;
import ru.pisarev.tm.exception.entity.ProjectNotFoundException;

public abstract class ProjectAbstractCommand extends AbstractCommand {

    protected void show(@Nullable final ProjectDto project) {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + Status.valueOf(project.getStatus().value()).getDisplayName());
    }

    @NotNull
    protected ProjectDto add(@Nullable final String name, @Nullable final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ProjectDto project = new ProjectDto();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Nullable
    protected String toString(ProjectDto project) {
        return project.getId() + ": " + project.getName();
    }

}
