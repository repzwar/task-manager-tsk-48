package ru.pisarev.tm.command.data;

import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.command.AuthAbstractCommand;

public class DataJsonSaveJaxBCommand extends AuthAbstractCommand {

    @Nullable

    public String name() {
        return "data-save-json-j";
    }

    @Nullable

    public String arg() {
        return null;
    }

    @Nullable

    public String description() {
        return "Save data to JSON by JaxB.";
    }

    public void execute() {
        serviceLocator.getDataEndpoint().saveDataJsonJaxB(getSession());
    }

}