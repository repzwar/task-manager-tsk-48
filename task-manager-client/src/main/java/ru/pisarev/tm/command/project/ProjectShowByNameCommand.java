package ru.pisarev.tm.command.project;

import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.command.ProjectAbstractCommand;
import ru.pisarev.tm.endpoint.ProjectDto;
import ru.pisarev.tm.exception.entity.ProjectNotFoundException;
import ru.pisarev.tm.util.TerminalUtil;

public class ProjectShowByNameCommand extends ProjectAbstractCommand {
    @Override
    public String name() {
        return "project-show-by-name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show project by name.";
    }

    @Override
    public void execute() {
        System.out.println("Enter name");
        @Nullable final String name = TerminalUtil.nextLine();
        @Nullable final ProjectDto project = serviceLocator.getProjectEndpoint().findProjectByName(getSession(), name);
        if (project == null) throw new ProjectNotFoundException();
        show(project);
    }
}
