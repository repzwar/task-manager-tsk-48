package ru.pisarev.tm.api.repository;

import ru.pisarev.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {


    Collection<AbstractCommand> getCommands();

    Collection<AbstractCommand> getArguments();

    Collection<String> getCommandNames();

    Collection<String> getCommandArg();

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArg(String arg);

    void add(AbstractCommand command);
}