package ru.pisarev.tm.repository.dto;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.repository.dto.IUserRecordRepository;
import ru.pisarev.tm.dto.UserRecord;

import javax.persistence.EntityManager;
import java.util.List;

public final class UserRecordRepository extends AbstractRecordRepository<UserRecord> implements IUserRecordRepository {

    public UserRecordRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    public List<UserRecord> findAll() {
        return entityManager.createQuery("SELECT e FROM UserRecord e", UserRecord.class).getResultList();
    }

    public UserRecord findById(@Nullable final String id) {
        return entityManager.find(UserRecord.class, id);
    }

    public void clear() {
        entityManager
                .createQuery("DELETE FROM UserRecord e")
                .executeUpdate();
    }

    public void removeById(@Nullable final String id) {
        UserRecord reference = entityManager.getReference(UserRecord.class, id);
        entityManager.remove(reference);
    }

    @Nullable
    @Override
    public UserRecord findByLogin(@Nullable final String login) {
        return getSingleResult(
                entityManager
                        .createQuery("SELECT e FROM UserRecord e WHERE e.login = :login", UserRecord.class)
                        .setHint(QueryHints.HINT_CACHEABLE, true)
                        .setParameter("login", login)
                        .setMaxResults(1)
        );
    }

    @Nullable
    @Override
    public UserRecord findByEmail(@Nullable final String email) {
        return getSingleResult(
                entityManager
                        .createQuery("SELECT e FROM UserRecord e WHERE e.email = :email", UserRecord.class)
                        .setHint(QueryHints.HINT_CACHEABLE, true)
                        .setParameter("email", email)
                        .setMaxResults(1)
        );
    }

    @Override
    public void removeUserByLogin(@Nullable final String login) {
        entityManager
                .createQuery("DELETE FROM UserRecord e WHERE e.login = :login")
                .setParameter(login, login)
                .executeUpdate();
    }

}