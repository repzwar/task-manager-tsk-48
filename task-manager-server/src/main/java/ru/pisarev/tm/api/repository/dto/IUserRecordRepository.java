package ru.pisarev.tm.api.repository.dto;

import ru.pisarev.tm.dto.UserRecord;

import java.util.List;

public interface IUserRecordRepository {

    UserRecord findByLogin(final String login);

    UserRecord findByEmail(final String email);

    void removeUserByLogin(final String login);

    void add(final UserRecord user);

    void update(final UserRecord user);

    List<UserRecord> findAll();

    UserRecord findById(final String id);

    void clear();

    void removeById(final String id);

}
