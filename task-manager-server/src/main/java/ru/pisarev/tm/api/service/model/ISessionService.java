package ru.pisarev.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.IService;
import ru.pisarev.tm.enumerated.Role;
import ru.pisarev.tm.model.SessionGraph;
import ru.pisarev.tm.model.UserGraph;

import java.util.List;

public interface ISessionService extends IService<SessionGraph> {

    SessionGraph open(@Nullable String login, @Nullable String password);

    UserGraph checkDataAccess(@Nullable String login, @Nullable String password);

    void validate(@NotNull SessionGraph session, Role role);

    void validate(@Nullable SessionGraph session);

    SessionGraph sign(@Nullable SessionGraph session);

    void close(@Nullable SessionGraph session);

    void closeAllByUserId(@Nullable String userId);

    @Nullable List<SessionGraph> findAllByUserId(@Nullable String userId);
}
