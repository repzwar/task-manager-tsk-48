package ru.pisarev.tm.api;


import ru.pisarev.tm.dto.AbstractRecord;

public interface IRecordService<E extends AbstractRecord> extends IRecordRepository<E> {

}