package ru.pisarev.tm.api.repository.model;

import ru.pisarev.tm.api.IRepository;
import ru.pisarev.tm.model.UserGraph;

public interface IUserRepository extends IRepository<UserGraph> {

    UserGraph findByLogin(final String login);

    UserGraph findByEmail(final String email);

    void removeUserByLogin(final String login);

    void update(final UserGraph user);

}
