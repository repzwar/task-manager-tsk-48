package ru.pisarev.tm.api.repository.model;

import ru.pisarev.tm.api.IRepository;
import ru.pisarev.tm.model.TaskGraph;

import java.util.List;

public interface ITaskRepository extends IRepository<TaskGraph> {

    List<TaskGraph> findAllTaskByProjectId(final String userId, final String projectId);

    void removeAllTaskByProjectId(final String userId, final String projectId);

    void bindTaskToProjectById(final String userId, final String taskId, final String projectId);

    void unbindTaskById(final String userId, final String id);

    void update(final TaskGraph task);

    TaskGraph findByIdUserId(final String userId, final String id);

    void clearByUserId(final String userId);

    void removeByIdUserId(final String userId, final String id);

    List<TaskGraph> findAllByUserId(final String userId);

    TaskGraph findByName(final String userId, final String name);

    TaskGraph findByIndex(final String userId, final int index);

    void removeByName(final String userId, final String name);

    void removeByIndex(final String userId, final int index);


}
