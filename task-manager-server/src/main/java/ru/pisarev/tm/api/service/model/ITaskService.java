package ru.pisarev.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.IService;
import ru.pisarev.tm.model.TaskGraph;
import ru.pisarev.tm.model.UserGraph;

import java.util.Collection;
import java.util.List;

public interface ITaskService extends IService<TaskGraph> {

    TaskGraph findByName(String userId, String name);

    TaskGraph findByIndex(String userId, Integer index);

    void removeByName(String userId, String name);

    void removeByIndex(String userId, Integer index);

    TaskGraph updateById(String userId, final String id, final String name, final String description);

    TaskGraph updateByIndex(String userId, final Integer index, final String name, final String description);

    TaskGraph startById(String userId, String id);

    TaskGraph startByIndex(String userId, Integer index);

    TaskGraph startByName(String userId, String name);

    TaskGraph finishById(String userId, String id);

    TaskGraph finishByIndex(String userId, Integer index);

    TaskGraph finishByName(String userId, String name);

    TaskGraph add(UserGraph user, String name, String description);

    List<TaskGraph> findAll(@NotNull String userId);

    void addAll(UserGraph user, @Nullable Collection<TaskGraph> collection);

    TaskGraph add(UserGraph user, @Nullable TaskGraph entity);

    TaskGraph findById(@NotNull String userId, @Nullable String id);

    void clear(@NotNull String userId);

    void removeById(@NotNull String userId, @Nullable String id);

    void remove(@NotNull String userId, @Nullable TaskGraph entity);
}
