package ru.pisarev.tm.api;


import ru.pisarev.tm.model.AbstractGraph;

import java.util.Collection;
import java.util.List;

public interface IService<E extends AbstractGraph> {

    List<E> findAll();

    E add(final E entity);

    void addAll(final Collection<E> collection);

    E findById(final String id);

    void clear();

    void removeById(final String id);

    void remove(final E entity);

}