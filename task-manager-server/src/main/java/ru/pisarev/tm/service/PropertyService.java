package ru.pisarev.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.IPropertyService;
import ru.pisarev.tm.api.other.ISignatureSetting;

import java.io.InputStream;
import java.util.Properties;

import static ru.pisarev.tm.constant.PropertyConst.*;

public final class PropertyService implements IPropertyService, ISignatureSetting {

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        if (System.getenv().containsKey(PASSWORD_SECRET_KEY))
            return System.getenv(PASSWORD_SECRET_KEY);
        if (System.getProperties().containsKey(PASSWORD_SECRET_KEY))
            return System.getProperty(PASSWORD_SECRET_KEY);
        return properties.getProperty(PASSWORD_SECRET_KEY, PASSWORD_SECRET_VALUE);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        if (System.getenv().containsKey(PASSWORD_ITERATION_KEY)) {
            @NotNull final String value = System.getenv(PASSWORD_ITERATION_KEY);
            return Integer.parseInt(value);
        }
        if (System.getProperties().containsKey(PASSWORD_ITERATION_KEY)) {
            @NotNull final String value = System.getProperty(PASSWORD_ITERATION_KEY);
            return Integer.parseInt(value);
        }
        @NotNull final String value = properties.getProperty(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_VALUE);
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        if (System.getenv().containsKey(APPLICATION_VERSION_KEY))
            return System.getenv(APPLICATION_VERSION_KEY);
        if (System.getProperties().containsKey(APPLICATION_VERSION_KEY))
            return System.getProperty(APPLICATION_VERSION_KEY);
        return properties.getProperty(APPLICATION_VERSION_KEY, APPLICATION_VERSION_VALUE);
    }

    @Override
    @NotNull
    public String getServerHost() {
        if (System.getenv().containsKey(SERVER_HOST_KEY))
            return System.getenv(SERVER_HOST_KEY);
        if (System.getProperties().containsKey(SERVER_HOST_KEY))
            return System.getProperty(SERVER_HOST_KEY);
        return properties.getProperty(SERVER_HOST_KEY, SERVER_HOST_VALUE);
    }

    @Override
    @NotNull
    public String getServerPort() {
        if (System.getenv().containsKey(SERVER_PORT_KEY))
            return System.getenv(SERVER_PORT_KEY);
        if (System.getProperties().containsKey(SERVER_PORT_KEY))
            return System.getProperty(SERVER_PORT_KEY);
        return properties.getProperty(SERVER_PORT_KEY, SERVER_PORT_VALUE);
    }

    @NotNull
    @Override
    public String getSignatureSecret() {
        if (System.getenv().containsKey(SIGNATURE_SECRET_KEY))
            return System.getenv(SIGNATURE_SECRET_KEY);
        if (System.getProperties().containsKey(SIGNATURE_SECRET_KEY))
            return System.getProperty(SIGNATURE_SECRET_KEY);
        return properties.getProperty(SIGNATURE_SECRET_KEY, SIGNATURE_SECRET_VALUE);
    }

    @NotNull
    @Override
    public Integer getSignatureIteration() {
        if (System.getenv().containsKey(SIGNATURE_ITERATION_KEY)) {
            @NotNull final String value = System.getenv(SIGNATURE_ITERATION_KEY);
            return Integer.parseInt(value);
        }
        if (System.getProperties().containsKey(SIGNATURE_ITERATION_KEY)) {
            @NotNull final String value = System.getProperty(SIGNATURE_ITERATION_KEY);
            return Integer.parseInt(value);
        }
        @NotNull final String value = properties.getProperty(SIGNATURE_ITERATION_KEY, SIGNATURE_ITERATION_VALUE);
        return Integer.parseInt(value);
    }

    @Override
    @Nullable
    public String getJdbcUser() {
        if (System.getenv().containsKey(JDBC_USER_KEY)) {
            @NotNull final String value = System.getenv(JDBC_USER_KEY);
            return value;
        }
        if (System.getProperties().containsKey(JDBC_USER_KEY)) {
            @NotNull final String value = System.getProperty(JDBC_USER_KEY);
            return value;
        }
        @Nullable final String value = properties.getProperty(JDBC_USER_KEY);
        return value;
    }

    @Override
    @Nullable
    public String getJdbcPassword() {
        if (System.getenv().containsKey(JDBC_PASSWORD_KEY)) {
            @NotNull final String value = System.getenv(JDBC_PASSWORD_KEY);
            return value;
        }
        if (System.getProperties().containsKey(JDBC_PASSWORD_KEY)) {
            @NotNull final String value = System.getProperty(JDBC_PASSWORD_KEY);
            return value;
        }
        @Nullable final String value = properties.getProperty(JDBC_PASSWORD_KEY);
        return value;
    }

    @Override
    @Nullable
    public String getJdbcUrl() {
        if (System.getenv().containsKey(JDBC_URL_KEY)) {
            @NotNull final String value = System.getenv(JDBC_URL_KEY);
            return value;
        }
        if (System.getProperties().containsKey(JDBC_URL_KEY)) {
            @NotNull final String value = System.getProperty(JDBC_URL_KEY);
            return value;
        }
        @Nullable final String value = properties.getProperty(JDBC_URL_KEY);
        return value;
    }

    @Override
    @Nullable
    public String getJdbcDriver() {
        if (System.getenv().containsKey(JDBC_DRIVER_KEY)) {
            @NotNull final String value = System.getenv(JDBC_DRIVER_KEY);
            return value;
        }
        if (System.getProperties().containsKey(JDBC_DRIVER_KEY)) {
            @NotNull final String value = System.getProperty(JDBC_DRIVER_KEY);
            return value;
        }
        @Nullable final String value = properties.getProperty(JDBC_DRIVER_KEY);
        return value;
    }

    @Override
    @Nullable
    public String getHibernateDialect() {
        if (System.getenv().containsKey(HIBERNATE_DIALECT_KEY)) {
            @NotNull final String value = System.getenv(HIBERNATE_DIALECT_KEY);
            return value;
        }
        if (System.getProperties().containsKey(HIBERNATE_DIALECT_KEY)) {
            @NotNull final String value = System.getProperty(HIBERNATE_DIALECT_KEY);
            return value;
        }
        @Nullable final String value = properties.getProperty(HIBERNATE_DIALECT_KEY);
        return value;
    }

    @Override
    @Nullable
    public String getHibernateBM2DDLAuto() {
        if (System.getenv().containsKey(HIBERNATE_HBM2DDL_AUTO_KEY)) {
            @NotNull final String value = System.getenv(HIBERNATE_HBM2DDL_AUTO_KEY);
            return value;
        }
        if (System.getProperties().containsKey(HIBERNATE_HBM2DDL_AUTO_KEY)) {
            @NotNull final String value = System.getProperty(HIBERNATE_HBM2DDL_AUTO_KEY);
            return value;
        }
        @Nullable final String value = properties.getProperty(HIBERNATE_HBM2DDL_AUTO_KEY);
        return value;
    }

    @Override
    @Nullable
    public String getHibernateShowSql() {
        if (System.getenv().containsKey(HIBERNATE_SHOW_SQL_KEY)) {
            @NotNull final String value = System.getenv(HIBERNATE_SHOW_SQL_KEY);
            return value;
        }
        if (System.getProperties().containsKey(HIBERNATE_SHOW_SQL_KEY)) {
            @NotNull final String value = System.getProperty(HIBERNATE_SHOW_SQL_KEY);
            return value;
        }
        @Nullable final String value = properties.getProperty(HIBERNATE_SHOW_SQL_KEY);
        return value;
    }

    @Override
    @Nullable
    public String getSecondLevelCash() {
        if (System.getenv().containsKey(CACHE_USE_SECOND_LEVEL_CACHE)) {
            @NotNull final String value = System.getenv(CACHE_USE_SECOND_LEVEL_CACHE);
            return value;
        }
        if (System.getProperties().containsKey(CACHE_USE_SECOND_LEVEL_CACHE)) {
            @NotNull final String value = System.getProperty(CACHE_USE_SECOND_LEVEL_CACHE);
            return value;
        }
        @Nullable final String value = properties.getProperty(CACHE_USE_SECOND_LEVEL_CACHE);
        return value;
    }

    @Override
    @Nullable
    public String getQueryCache() {
        if (System.getenv().containsKey(CACHE_USE_QUERY_CACHE)) {
            @NotNull final String value = System.getenv(CACHE_USE_QUERY_CACHE);
            return value;
        }
        if (System.getProperties().containsKey(CACHE_USE_QUERY_CACHE)) {
            @NotNull final String value = System.getProperty(CACHE_USE_QUERY_CACHE);
            return value;
        }
        @Nullable final String value = properties.getProperty(CACHE_USE_QUERY_CACHE);
        return value;
    }

    @Override
    @Nullable
    public String getMinimalPuts() {
        if (System.getenv().containsKey(CACHE_USE_MINIMAL_PUTS)) {
            @NotNull final String value = System.getenv(CACHE_USE_MINIMAL_PUTS);
            return value;
        }
        if (System.getProperties().containsKey(CACHE_USE_MINIMAL_PUTS)) {
            @NotNull final String value = System.getProperty(CACHE_USE_MINIMAL_PUTS);
            return value;
        }
        @Nullable final String value = properties.getProperty(CACHE_USE_MINIMAL_PUTS);
        return value;
    }

    @Override
    @Nullable
    public String getLiteMember() {
        if (System.getenv().containsKey(CACHE_HAZELCAST_USE_LITE_MEMBER)) {
            @NotNull final String value = System.getenv(CACHE_HAZELCAST_USE_LITE_MEMBER);
            return value;
        }
        if (System.getProperties().containsKey(CACHE_HAZELCAST_USE_LITE_MEMBER)) {
            @NotNull final String value = System.getProperty(CACHE_HAZELCAST_USE_LITE_MEMBER);
            return value;
        }
        @Nullable final String value = properties.getProperty(CACHE_HAZELCAST_USE_LITE_MEMBER);
        return value;
    }

    @Override
    @Nullable
    public String getRegionPrefix() {
        if (System.getenv().containsKey(CACHE_REGION_PREFIX)) {
            @NotNull final String value = System.getenv(CACHE_REGION_PREFIX);
            return value;
        }
        if (System.getProperties().containsKey(CACHE_REGION_PREFIX)) {
            @NotNull final String value = System.getProperty(CACHE_REGION_PREFIX);
            return value;
        }
        @Nullable final String value = properties.getProperty(CACHE_REGION_PREFIX);
        return value;
    }

    @Override
    @Nullable
    public String getCacheProvider() {
        if (System.getenv().containsKey(CACHE_PROVIDER_CONFIGURATION_FILE_RESOURCE_PATH)) {
            @NotNull final String value = System.getenv(CACHE_PROVIDER_CONFIGURATION_FILE_RESOURCE_PATH);
            return value;
        }
        if (System.getProperties().containsKey(CACHE_PROVIDER_CONFIGURATION_FILE_RESOURCE_PATH)) {
            @NotNull final String value = System.getProperty(CACHE_PROVIDER_CONFIGURATION_FILE_RESOURCE_PATH);
            return value;
        }
        @Nullable final String value = properties.getProperty(CACHE_PROVIDER_CONFIGURATION_FILE_RESOURCE_PATH);
        return value;
    }

    @Override
    @Nullable
    public String getFactoryClass() {
        if (System.getenv().containsKey(CACHE_REGION_FACTORY_CLASS)) {
            @NotNull final String value = System.getenv(CACHE_REGION_FACTORY_CLASS);
            return value;
        }
        if (System.getProperties().containsKey(CACHE_REGION_FACTORY_CLASS)) {
            @NotNull final String value = System.getProperty(CACHE_REGION_FACTORY_CLASS);
            return value;
        }
        @Nullable final String value = properties.getProperty(CACHE_REGION_FACTORY_CLASS);
        return value;
    }

}
