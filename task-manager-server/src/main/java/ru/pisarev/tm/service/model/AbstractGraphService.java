package ru.pisarev.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.api.service.IConnectionService;
import ru.pisarev.tm.model.AbstractGraph;

public abstract class AbstractGraphService<E extends AbstractGraph> {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    public AbstractGraphService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
