package ru.pisarev.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.enumerated.Status;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Setter
@Getter
@Entity
@Cacheable
@NoArgsConstructor
@Table(name = "tm_project")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ProjectGraph extends AbstractBusinessGraph {

    @Column
    @NotNull
    private String name;

    @Column
    @Nullable
    private String description;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @Column(name = "start_date")
    private Date startDate;

    @Nullable
    @Column(name = "finish_date")
    private Date finishDate;

    @Column
    @NotNull
    private Date created = new Date();

    @Nullable
    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<TaskGraph> tasks = new ArrayList<>();

    @NotNull
    public ProjectGraph(@NotNull String name) {
        this.name = name;
    }

    @NotNull
    public ProjectGraph(@NotNull String name, @Nullable String description) {
        this.name = name;
        this.description = description;
    }


}