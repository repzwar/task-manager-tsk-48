package ru.pisarev.tm.constant;

public interface BackupConst {

    String BACKUP_SAVE = "backup-save";

    String BACKUP_LOAD = "backup-load";

}
